# Microsoft Word Comments Macro

A macro that strips comments made with Word's comment facility out into a csv spreadsheet. 

Instructions: 
1. clone or download the comments macro at: https://gitlab.com/moortec-miscellaneous/ms-vba/comments-macro.git
2. Alt+F11 to open the VBA editor screen
3. File >> Import the comments-macro.bas file
4. Select Run, and Save the Spreadsheet when prompted.

