Attribute VB_Name = "CommentMacro"
'Modified by Giles Gamon to align columns with DBS QRR Form.
'Updated by Peter Burke 31-JUL-14 to better reflect the format of the comment log currently in use.
'Updated by Peter Burke 20-OCT-14 to remove "page" from the page number as the validation rules on the
'new comment log will only allow integers.
'Updated by Peter Burke 5-APR-16 now colour the quoted text blue, and changed the output columns for NHSBSA.
'Updated by Peter Burke 10-JUL-16 Added H/M/L priority column.

Option Explicit

'Define type for heading number array
Public Type CommentOutput
    strHeadingNum As String
    iCommIndex As Integer
End Type

Public Sub ExtractCommentsToNewExecl()

    Dim oDesignDoc As Document
    Dim oHeaderTable As Table
    Dim nCount As Long
    Dim X As Long
    Dim Title As String
    Dim strDocName As String
    Dim iPos As Integer
    Dim Para As Paragraph
    Dim nCommCount As Long
    Dim strReviewerInitials As String
    Dim strHeadingNumber As String
    Dim coDesignDocHeadings(999) As CommentOutput
    Dim xlApp As Object
    Dim xlWB As Object
    Dim strExcelName As String
    Dim i As Integer

    Title = "Extract All Comments to New Excel Workbook - NHSBSA Version"

    'Set the input document as active
    Set oDesignDoc = ActiveDocument

    'Get the total number of comments
    nCount = ActiveDocument.Comments.Count

    If nCount = 0 Then
        MsgBox "The active document contains no comments.", vbOKOnly, Title
        GoTo ExitHere
    Else
        'Stop if user does not click Yes
        If MsgBox("Do you want to extract all comments to a NHSBSA formated review spreadsheet?", _
                vbYesNo + vbQuestion, Title) <> vbYes Then
            GoTo ExitHere
        End If
    End If


    Application.ScreenUpdating = True

    StatusBar = "Extracting Heading numbers. Please wait..."

    Application.ScreenRefresh

    ' create a new workbook
    Set xlApp = CreateObject("Excel.Application")
    xlApp.Visible = True
    Set xlWB = xlApp.Workbooks.Add

    ' run through the source and create an array for the section numbers (for each comment)
    For Each Para In oDesignDoc.Paragraphs
        ' Check for Heading style
        If Para.Format.Style Like "Heading [0-9]*" Then
            'Get the Heading Number
            strHeadingNumber = Para.Range.ListFormat.ListString
        End If
        'Check number of comments in the paragraph

        If Para.Range.Comments.Count > 0 Then
            'Write the heading number in the array for every comment in the paragraph
            For X = 1 To Para.Range.Comments.Count
                nCommCount = nCommCount + 1
                coDesignDocHeadings(nCommCount).strHeadingNum = strHeadingNumber
                coDesignDocHeadings(nCommCount).iCommIndex = nCommCount
            Next X
        End If

    Next Para
 
    Application.ScreenUpdating = True
    StatusBar = "Creating Comment Document. Please wait..."
    Application.ScreenRefresh

    'Get info from each comment from oDesignDoc and insert in speadsheet
    ' TODO: it would be helpful to be able to make the referenced text a different
    ' colour to the actual comment. This is possible in Excel, http://vbadud.blogspot.co.uk/2007/09/excel-vba-change-font-color-for-part-of.html
    ' has some good examples of how to do it in VBA.
    With xlWB.Application
        ' For each column add the appropriate information
        For X = 1 To nCount
            .Cells(X, 1).Value = ""                                                                     ' Column A not used
            .Cells(X, 2).Value = oDesignDoc.Comments(X).Author & "." & Format(X, "0000")                ' B: Comment ID
            .Cells(X, 3).Value = oDesignDoc.Name                                                        ' C: The document name
            .Cells(X, 4).Value = "Pending"                                                              ' D: Pending is the default for all new comments.
            .Cells(X, 5).Value = "" & oDesignDoc.Comments(X).Scope.Information(wdActiveEndPageNumber)   ' E: Page Number
            .Cells(X, 6).Value = "Section " & coDesignDocHeadings(X).strHeadingNum                      ' F: Section Ref
'            .Cells(X, 7).Value = "<''" & oDesignDoc.Comments(X).Scope & "'> -- " & oDesignDoc.Comments(X).Range.text
            .Cells(X, 7).Value = "<QUOTE>''" & oDesignDoc.Comments(X).Scope & "''</QUOTE> -- " & oDesignDoc.Comments(X).Range.text
            .Cells(X, 7).Characters(1, Len(oDesignDoc.Comments(X).Scope) + 19).Font.Color = vbBlue
            .Cells(X, 8).Value = "H/M/L"
            .Cells(X, 9).Value = oDesignDoc.Comments(X).Author                                          ' I: Raised By
        Next X

        .Rows("1:" & nCount).Select
        .Selection.Font.Name = "Arial Narrow"
        .Selection.Font.Size = 10
    End With


    ' Change the size / formatting to match NHSBSA standard.
    xlWB.Application.Columns("A").ColumnWidth = 10
    xlWB.Application.Columns("B").ColumnWidth = 17
    xlWB.Application.Columns("C").ColumnWidth = 30
    xlWB.Application.Columns("C").WrapText = True
    xlWB.Application.Columns("D").ColumnWidth = 10
    xlWB.Application.Columns("E").ColumnWidth = 10
    xlWB.Application.Columns("G").ColumnWidth = 50
    xlWB.Application.Columns("G").WrapText = True

    ' Add borders so these don't mess up the formatting when the comments are cut-and-pasted.
    xlWB.Application.Range("B1:I" & nCount).Borders.Weight = xlSingle
    xlWB.Application.Range("B1:I" & nCount).VerticalAlignment = xlCenter
    xlWB.Application.Range("B1:I" & nCount).HorizontalAlignment = xlCenter

    ' Select all the added data for ease of cutting and pasting into the 'actual' comments sheet.
    xlWB.Application.Range("A1:I" & nCount).Select
 
    Application.ScreenUpdating = True
    StatusBar = "Saving...."
    Application.ScreenRefresh
 
    'Find position of extension in filename
    strDocName = oDesignDoc.FullName
    iPos = InStrRev(strDocName, ".doc")

    'Generate new full path name with all the reviewers initials appended
    strDocName = Left(strDocName, iPos - 1)
    strDocName = strDocName & " - " & strReviewerInitials & " review form.doc"

    'Save new file with new name
    'ActiveDocument.SaveAs FileName:=strDocName, _
    '    FileFormat:=wdFormatDocument
  
   'Generate new full path name with reviewers initials appended
    strExcelName = Left(strDocName, iPos - 1)
    strExcelName = strExcelName & strReviewerInitials & " review form.xlsx"

    xlWB.SaveAs (strExcelName)
  
'    xlApp.Quit ' close the Excel application
    Set xlWB = Nothing
    Set xlApp = Nothing
   
    Application.ScreenUpdating = True
    StatusBar = "Finished."
    Application.ScreenRefresh
  
    MsgBox nCount & " comments found. Finished creating comments Excel Workbook.", vbOKOnly + vbMsgBoxSetForeground, Title
    oDesignDoc.Application.Quit

ExitHere:
    Set oDesignDoc = Nothing
    Set oHeaderTable = Nothing
End Sub

' Todo: cope with [] in file names.
